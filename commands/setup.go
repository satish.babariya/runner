package commands

import (
	"bufio"
	"gitlab-runner-v0.1.7/common"
	"os"
	"strings"
	log "github.com/Sirupsen/logrus"
	"github.com/codegangsta/cli"
)

func ask(r *bufio.Reader, prompt string, result *string, allow_empty ...bool) {
	for len(*result) == 0 {
		println(prompt)
		data, _, err := r.ReadLine()
		if err != nil {
			panic(err)
		}
		*result = string(data)
		*result = strings.TrimSpace(*result)

		if len(allow_empty) > 0 && allow_empty[0] && len(*result) == 0 {
			return
		}
	}
}

func askExecutor(r *bufio.Reader, result *string) {
	for {
		ask(r, "Please enter the executor: shell?", result)
		if common.GetExecutor(*result) != nil {
			return
		}
	}
}




func runSetup(c *cli.Context) {
	file, err := os.OpenFile(c.String("config"), os.O_APPEND|os.O_CREATE, 0600)
	if file != nil {
		file.Close()
	}

	config := common.Config{}
	err = config.LoadConfig(c.String("config"))
	if err != nil {
		panic(err)
	}

	url := c.String("url")
	registrationToken := c.String("registration-token")
	description := c.String("description")
	tags := c.String("tag-list")

	bio := bufio.NewReader(os.Stdin)
	ask(bio, "Please enter the gitlab-ci coordinator URL (e.g. http://gitlab-ci.org:3000/ )", &url)
	ask(bio, "Please enter the gitlab-ci token for this runner", &registrationToken)
	ask(bio, "Please enter the gitlab-ci description for this runner", &description)
	// ask(bio, "Please enter the tag list separated by comma or leave it empty", &tags, true)

	result := common.RegisterRunner(url, registrationToken, description, tags)
	if result == nil {
		log.Fatalf("Failed to register this runner. Perhaps your SSH key is invalid or you are having network problems")
	}

	runner_config := common.RunnerConfig{
		URL:      url,
		Name:     description,
		Token:    result.Token,
		Executor: c.String("executor"),
	}

	askExecutor(bio, &runner_config.Executor)

	config.Runners = append(config.Runners, &runner_config)

	err = config.SaveConfig(c.String("config"))
	if err != nil {
		panic(err)
	}

	log.Printf("Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!")
}

var (
	CmdRunSetup = cli.Command{
		Name:      "setup",
		ShortName: "s",
		Usage:     "setup a new runner",
		Action:    runSetup,
		Flags: []cli.Flag{
			cli.StringFlag{
				Name:   "registration-token",
				Value:  "xxxxxxtoken",
				Usage:  "Runner's registration token",
				EnvVar: "REGISTRATION_TOKEN",
			},
			cli.StringFlag{
				Name:   "url",
				Value:  "https://hook.io/satishbabariya/runner",
				Usage:  "Runner URL",
				EnvVar: "CI_SERVER_URL",
			},
			cli.StringFlag{
				Name:   "description",
				Value:  "xxxdescription",
				Usage:  "Runner's registration description",
				EnvVar: "RUNNER_DESCRIPTION",
			},
			cli.StringFlag{
				Name:   "config",
				Value:  "config.toml",
				Usage:  "Config file",
				EnvVar: "CONFIG_FILE",
			},
			cli.StringFlag{
				Name:   "tag-list",
				Value:  "",
				Usage:  "Runner's tag list separated by comma",
				EnvVar: "RUNNER_TAG_LIST",
			},
			cli.StringFlag{
				Name:   "executor",
				Value:  "shell",
				Usage:  "Select executor, eg. shell, docker, etc.",
				EnvVar: "RUNNER_EXECUTOR",
			},
		},
	}
)
