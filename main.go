package main

import (
	"gitlab-runner-v0.1.7/commands"

	"path"
	"github.com/codegangsta/cli"
	"os"

	log "github.com/Sirupsen/logrus"


)

func main() {
	app := cli.NewApp()
	app.Name = path.Base(os.Args[0])
	app.Usage = "a GitLab-CI Multi Runner"
	app.Version = commands.VERSION

	app.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:   "debug",
			Usage:  "debug mode",
			EnvVar: "DEBUG",
		},
		cli.StringFlag{
			Name:  "log-level, l",
			Value: "info",
			Usage: "Log level (options: debug, info, warn, error, fatal, panic)",
		},
	}

	// logs
	app.Before = func(c *cli.Context) error {
		log.SetOutput(os.Stderr)
		level, err := log.ParseLevel(c.String("log-level"))
		if err != nil {
			log.Fatalf(err.Error())
		}
		log.SetLevel(level)

		// If a log level wasn't specified and we are running in debug mode,
		// enforce log-level=debug.
		if !c.IsSet("log-level") && !c.IsSet("l") && c.Bool("debug") {
			log.SetLevel(log.DebugLevel)
		}
		return nil
	}

	app.Commands = []cli.Command{
		//commands.CmdRunMulti,
		commands.CmdRunSetup,
		commands.CmdRunSingle,
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
